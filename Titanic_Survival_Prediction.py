#!/usr/bin/env python
# coding: utf-8

# # Titanic Survival Prediction
# #By- Aarush Kumar
# #Dated: June 22,2021

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import tree,svm
from sklearn.metrics import accuracy_score


# In[2]:


df = pd.read_csv('/home/aarush100616/Downloads/Projects/Titanic Survival/train.csv')


# In[3]:


df


# In[4]:


df.head()


# In[5]:


df.info()


# In[6]:


df.isnull().sum()


# In[7]:


df.describe()


# In[9]:


heatmap = sns.heatmap(df[["Survived", "SibSp", "Parch", "Age", "Fare"]].corr(), annot = True)
sns.set(rc={'figure.figsize':(15,15)})


# ## EDA

# ### Number of Siblings

# In[10]:


df['SibSp'].unique()


# In[12]:


bargraph_sibsp = sns.catplot(x = "SibSp", y = "Survived", data = df, kind="bar", height = 8)


# In[14]:


ageplot = sns.FacetGrid(df, col="Survived", height = 7)
ageplot = ageplot.map(sns.distplot, "Age")
ageplot = ageplot.set_ylabels("Survival Probability")


# ###  Conclusion: More age -> less chances of survival!

# In[15]:


sexplot = sns.barplot(x="Sex", y="Survived", data=df)


# In[16]:


pclassplot = sns.catplot(x = "Pclass", y="Survived", data = df, kind="bar", height = 6)


# In[17]:


a = sns.catplot(x = "Pclass", y="Survived", hue="Sex", data=df, height = 7, kind="bar")


# In[18]:


df["Embarked"].isnull().sum()


# In[19]:


df["Embarked"].value_counts()


# In[20]:


df["Embarked"] = df["Embarked"].fillna('S')


# In[21]:


sns.catplot(x="Embarked", y="Survived", data=df, height = 5, kind="bar")


# In[22]:


sns.catplot(x="Pclass", col="Embarked", data = df, kind="count", height=7)


# In[23]:


mean = df["Age"].mean()
std = df["Age"].std()
print(mean)
print(std)


# In[24]:


rand_age = np.random.randint(mean-std, mean+std, size = 177)
age_slice = df["Age"].copy()
age_slice[np.isnan(age_slice)] = rand_age
df["Age"] = age_slice
df.isnull().sum()


# In[25]:


col_to_drop = ["PassengerId", "Ticket", "Cabin", "Name"]
df.drop(col_to_drop, axis=1, inplace=True)
df.head(10)


# ## Converting Categorical Variables to Numeric

# In[26]:


genders = {"male":0, "female":1}
df["Sex"] = df["Sex"].map(genders)
ports = {"S":0, "C":1, "Q":2}
df["Embarked"] = df["Embarked"].map(ports)
df.head()


# In[27]:


df_train_x = df[['Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare', 'Embarked']]
# Target variable column
df_train_y = df[['Survived']]
x_train, x_test, y_train, y_test = train_test_split(df_train_x, df_train_y, test_size=0.20, random_state=42)


# In[28]:


clf1 = RandomForestClassifier()
clf1.fit(x_train, y_train)
rfc_y_pred = clf1.predict(x_test)
rfc_accuracy = accuracy_score(y_test,rfc_y_pred) * 100
print("accuracy=",rfc_accuracy)


# In[29]:


clf2 = LogisticRegression()
clf2.fit(x_train, y_train)
lr_y_pred = clf2.predict(x_test)
lr_accuracy = accuracy_score(y_test,lr_y_pred)*100
print("accuracy=",lr_accuracy)


# In[30]:


clf3 = KNeighborsClassifier(5)
clf3.fit(x_train, y_train)
knc_y_pred = clf3.predict(x_test)
knc_accuracy = accuracy_score(y_test,knc_y_pred)*100
print("accuracy=",knc_accuracy)


# In[31]:


clf4 = tree.DecisionTreeClassifier()
clf4 = clf4.fit(x_train, y_train)
dtc_y_pred = clf4.predict(x_test)
dtc_accuracy = accuracy_score(y_test,dtc_y_pred)*100
print("accuracy=",dtc_accuracy)


# In[32]:


clf5 = svm.SVC()
clf5.fit(x_train, y_train)
svm_y_pred = clf5.predict(x_test)
svm_accuracy = accuracy_score(y_test,svm_y_pred)*100
print("accuracy=",svm_accuracy)


# In[33]:


print("Accuracy of Random Forest Classifier =",rfc_accuracy)
print("Accuracy of Logistic Regressor =",lr_accuracy)
print("Accuracy of K-Neighbor Classifier =",knc_accuracy)
print("Accuracy of Decision Tree Classifier = ",dtc_accuracy)
print("Accuracy of Support Vector Machine Classifier = ",svm_accuracy)


# In[34]:


# Importing test.csv
df1 = pd.read_csv('/home/aarush100616/Downloads/Projects/Titanic Survival/test.csv')
df1.head(10)


# In[35]:


df1.info()


# In[36]:


df1.isnull().sum()


# In[37]:


# Replacing missing values of age column
mean = df1["Age"].mean()
std = df1["Age"].std()
rand_age = np.random.randint(mean-std, mean+std, size = 86)
age_slice = df1["Age"].copy()
age_slice[np.isnan(age_slice)] = rand_age
df1["Age"] = age_slice


# In[38]:


df1['Fare'].fillna(df1['Fare'].mean(), inplace=True)
df1.isnull().sum()


# In[39]:


col_to_drop = ["PassengerId", "Ticket", "Cabin", "Name"]
df1.drop(col_to_drop, axis=1, inplace=True)
df1.head(10)


# In[40]:


genders = {"male":0, "female":1}
df1["Sex"] = df1["Sex"].map(genders)
ports = {"S":0, "C":1, "Q":2}
df1["Embarked"] = df1["Embarked"].map(ports)
df1.head()


# In[43]:


x_test = df1
y_pred = clf1.predict(x_test)
originaltest_data = pd.read_csv('/home/aarush100616/Downloads/Projects/Titanic Survival/test.csv')
submission = pd.DataFrame({
        "PassengerId": originaltest_data["PassengerId"],
        "Survived": y_pred
    })
submission

